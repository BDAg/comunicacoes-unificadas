import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Axios from 'axios';
import { LoginContext } from '../../pages/LoginContext';
import './Brand.css';


class Brand extends Component {
    zerarToken() {
        localStorage.removeItem('Token');
        }
            render() {
                return (
                    <div>
                        <a href="#/home">
                            <img src="/img/comunica.png" class="logo" alt="Logo life"/> 
                        </a>
                        <div class="brand">
                            <div className='navUser'>
                                <div className='navText'>
                                    <label>Bem vindo {this.context.name}</label>
                                    <NavLink to="/" activeClassName="selected" onClick={this.zerarToken.bind(this)}>Logout</NavLink>
                                </div>
                                <i class="fas fa-user-circle icon"></i>
                            </div>
                        </div>
                    </div>
                );
            }
        }
Brand.contextType = LoginContext;
export default Brand;