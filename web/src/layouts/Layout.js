import React, { Component } from 'react';

import Brand from './Header/Brand';
import Navbar from './Navbar/Navbar';
import Alert from '../components/Alert'

class Layout extends Component {
    render() {
        return(
            <div>
                <header>
                    <Brand/>
                    <Navbar/>
                </header>
                {this.props.children}
                <Alert/>
            </div>
        );
    }
}

export default Layout;