import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css";
import { LoginContext } from "../../pages/LoginContext";

class Navbar extends Component {
    render() {
        return (
            <nav>
                <div className="nav-item">
                    <NavLink to="/home" activeClassName="selected">Home</NavLink>
                </div>
                <div className="nav-item">
                    <NavLink to="/chat" activeClassName="selected">Chat</NavLink>
                </div>
                <div className="nav-item">
                    <NavLink to="/phone" activeClassName="selected">Phone</NavLink>
                </div>
                {this.context.access >= 3 ? (
                    <div className="nav-item drop">
                        <NavLink
                            to="#"
                            class="dropbtn"
                            to="#"
                            isActive={(match, location) => {
                                if (location.pathname == "/user" || location.pathname == "/access")
                                    return true;
                                else return false;
                            }}
                            activeClassName="selected"
                        >
                        Usuários
                        </NavLink>
                        <div className="dropdown-content">
                            <NavLink to="/user" activeClassName="selected">Lista de Usuários</NavLink>
                            <NavLink to="/access" activeClassName="selected">Níveis de Acesso</NavLink>
                        </div>
                    </div>
                    ) : '' 
                }
                {this.context.access >= 2 ? (
                    <div className="nav-item">
                        <a target="_blank" href="http://betatest.cloudpabx.life.com.br/">
                            PBX
                        </a>
                    </div>
                    ) : ''    
                }
            </nav>
        );
    }
}
Navbar.contextType = LoginContext;
export default Navbar;
