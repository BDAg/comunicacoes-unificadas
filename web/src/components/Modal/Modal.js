import React from 'react';
import Modal from 'react-modal';
import './Modal.css';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: '50%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root');

class ModalEdit extends React.Component {
    constructor() {
        super();

        this.state = {
            modalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = 'black';
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    render() {
        return (
            <div>
                <a className="modalTitle" onClick={this.openModal} >{this.props.title}</a>

                <Modal isOpen={this.state.modalIsOpen}
                onAfterOpen = { this.afterOpenModal }
                onRequestClose = { this.closeModal }
                style = { customStyles }
                contentLabel = "Example Modal">
                    <h2 ref={subtitle => this.subtitle = subtitle}></h2>
                {this.props.children}
                <button className="closeModal" onClick={this.closeModal} > Fechar </button>

                </Modal> 
            </div>
        );
    }
}

export default ModalEdit;