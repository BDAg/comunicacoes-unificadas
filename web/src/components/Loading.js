import React, { Component } from 'react';
import LoadingScreen from 'react-loading-screen'
 

class Loading extends Component {

    constructor() {
        super();

        this.state = {
            loading: undefined
        };
    }

    componentWillMount(){
        this.setState({
            loading: this.props.loaded
        })
    }

    render(){
        return(
            <LoadingScreen
                loading={this.state.loading}
                bgColor='#282a2c'
                spinnerColor='#EF7917'
                textColor='#676767'
                > 
            </LoadingScreen>
        );
    }
}

export default Loading;