import React, { Component } from 'react';
import Axios from 'axios';
import Modal from '../Modal/Modal';
import EditUser  from './EditUser';
import AddUser  from './AddUser';
import { toast } from 'react-toastify';
import EditPassword from './EditPassword';
import { LoginContext } from '../../pages/LoginContext';

class ListarUsuarios extends Component{

    constructor(props){
        super(props);
        this.state = {
            usuarios: [],
            access: [],
            id: null,
        };

    }

    getUsers(){
        Axios({
            method: 'GET',
            url: 'https://ucs.api.life.com.br/user',
            headers: {'Token':localStorage.getItem('Token')}
        }).then((response) => {
            this.setState({
                usuarios: response.data,
            })
        }).catch((err) => {
            toast.error(err.message)
        });
    }

    editAccess(value, field, user){
        user[field] = value;
        if(window.confirm('Deseja mesmo alterar o nível de acesso deste usuario?')) {    
                Axios({
                method: 'PUT',
                url: `https://ucs.api.life.com.br/user/${user.id}`,
                data: user,
                headers: {'Token':localStorage.getItem('Token')}
            }).then((response) => {
                toast.success(response.data.message)
                this.getUsers();   
            }).catch((error) => {
                toast.error(error.message)
            })         
        }
    }
    
    
    getAccess(){
        Axios({
            method: 'GET',
            url: 'https://ucs.api.life.com.br/access',
            headers: {'token':localStorage.getItem('Token')}
        }).then((response) => {
            this.setState({
                access: response.data
            })
        }).catch((err) => {
            toast.error(err.message)
        });
    }

    deleteUser(idUser){
        if (window.confirm('Deseja mesmo remover este usuário?')) {
            Axios({
                method: 'DELETE',
                url: `https://ucs.api.life.com.br/user/${idUser}`,
                headers: {'Token':localStorage.getItem('Token')}
            }).then((response) => {
                toast.success(response.data.message);
                this.getUsers();
            }).catch((error) => {
                toast.error(error.message)
            });
            this.getUsers();
        }
    }

    blockUser(value, field, user){
        user[field] = value;
        if (window.confirm('Deseja mesmo bloquear este usuario?')) {    
                Axios({
                method: 'PUT',
                url: `https://ucs.api.life.com.br/user/active/${user.id}`,
                data: user,
                headers: {'Token':localStorage.getItem('Token')}
            }).then((response) => {
                toast.success(response.data.message)
                this.getUsers();   
            }).catch((error) => {
                toast.error(error.message)
            })         
        }
    }

    componentDidMount() {
        this.getUsers();
        this.getAccess();
    }


    render(){
        return(
            <div class="listContainer">
                <h1>Usuários</h1>
                <label><Modal title={<i className="fas fa-user addbutton"/>}><AddUser /></Modal></label>
                <table>
                    <thead>
                        <th>ID</th>
                        <th>Nome Usuario</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th>Níveis de acesso</th>
                    </thead>
                    <tbody>
                        {this.state.usuarios.map(usuario => 
                            <tr>
                                <td>{usuario.id}</td>
                                <td>{usuario.name}</td>
                                <td>{usuario.telephone}</td>
                                <td>{usuario.email}</td>                            
                                <td>
                                    {this.context.access >=3 ?
                                        <select value={usuario.level_id} onChange={(event) => this.editAccess(event.target.value, 'level_id', usuario)}>
                                            {this.state.access.map(access =>
                                                <option value={access.id}>{access.access}</option> 
                                            )}
                                        </select>: 
                                        <select value={usuario.level_id} >
                                            {this.state.access.map(access =>
                                                <option value={access.id}>{access.access}</option> 
                                            )}
                                        </select>
                                    }
                                </td>
                                <td ><Modal title="Trocar Senha"><EditPassword user={usuario.id}/></Modal></td>
                                <td><Modal title={<i class="far fa-edit tablebutton"/>}><EditUser user={usuario.id}/></Modal></td>
                                {usuario.active == 1?
                                    <td><a type="submit" onClick={() => this.blockUser(0, 'active', usuario)}><i className="fas fa-unlock tablebutton"/></a></td>
                                    : 
                                    <td><a type="submit" onClick={() => this.blockUser(1, 'active', usuario)}><i className="fas fa-lock tablebutton"/></a></td>
                                }    
                                <td><a type="submit" onClick={() => this.deleteUser(usuario.id)}><i className="fas fa-trash-alt tablebutton"/></a></td> 
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}

ListarUsuarios.contextType = LoginContext;
export default ListarUsuarios;