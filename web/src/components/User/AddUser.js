import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';
import './User.css';

class AddUsuarios extends Component{

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            telephone: '',
            password: '',
            level_id: 1
            
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.addUser();
        this.setState({
            name: '',
            email: '',
            telephone: '',
            password: '',
            level_id: 1
        });
        document.getElementById('name').value = '';
        document.getElementById('email').value = '';
        document.getElementById('telephone').value = '';
        document.getElementById('password').value = '';
        document.getElementById('level_id').value = '1';
        
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    

    addUser() {
        if(window.confirm('Deseja adicionar este usuário?')) {
            Axios({
                method: 'POST',
                url: 'https://ucs.api.life.com.br/user/',
                data: this.state,
                headers: {'Token':localStorage.getItem('Token')},
            }).then((response) => {
                toast.success(response.data.message);
            }).catch((error) => {
                toast.error(error.message);
            });
        }
    }

    render(){
        return(
            <div>
                <h1 class="user-add-h1">Adicionar Usuário</h1>
                <form id="form" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Nome</label>
                        <input name="name" id="name" defaultValue={this.state.name} onChange={this.handleChange} required/>
                    </div>

                    <div className="form-group">
                        <label>Email</label>
                        <input name="email" id="email" defaultValue={this.state.email} onChange={this.handleChange} required/>
                    </div>

                    <div className="form-group">
                        <label>Telefone</label>
                        <input name="telephone" id="telephone" defaultValue={this.state.telephone} onChange={this.handleChange} required/>
                    </div>

                    <div className="form-group">
                        <label>Nível de Acesso</label>
                        <select name="level_id" id="level_id" defaultValue={this.state.level_id} onChange={this.handleChange} required>
                            <option value="1">Colaborador</option>
                            <option value="2">Moderador</option>
                            <option value="3">Administrador</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Senha</label>
                        <input type="password" name="password" id="password" defaultValue={this.state.password} onChange={this.handleChange} required/>
                    </div>

                    <button className="button bg-graphite" type="submit">Enviar</button>
                </form>
            </div>
        );
    }
}

export default AddUsuarios;