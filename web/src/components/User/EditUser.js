import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify'

class EditUsuarios extends Component{

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            telephone: '',
            level_id: null

        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.EditUser();
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
        console.log(this.state)
    }
    componentDidMount() {
        this.getUser();
    }

    EditUser() {

        if(window.confirm('Deseja Editar este usuário?')) {
            Axios({
                method: 'PUT',
                url: `https://ucs.api.life.com.br/user/${this.props.user}`,
                data: this.state,
                headers: {'Token':localStorage.getItem('Token')}
            }).then((response) => {
                toast.success(response.data.message)
            }).catch((error) => {
                toast.error(error.message)
            });
        }
    }

    getUser() {
        Axios({
            method: 'GET',
            url: `https://ucs.api.life.com.br/user/${this.props.user}`,
            headers: { 'Token': localStorage.getItem('Token')}
        }).then((response) => {
            this.setState({
                name: response.data.name,
                email: response.data.email,
                telephone: response.data.telephone,
                level_id: response.data.level_id
            })
            console.log(this.state)
        }).catch((err) => {
            toast.error(err.message)
        });
    }

    render(){
        return(
            <div>
                <h1>Editar Usuário</h1>
                <form id="form" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Nome</label>
                        <input name="name" defaultValue={this.state.name} onChange={this.handleChange} required />
                        
                    </div>

                    <div className="form-group">
                        <label>Email</label>
                        <input name="email" defaultValue={this.state.email} onChange={this.handleChange} required/>
                    </div>

                    <div className="form-group">
                        <label>Telefone</label>
                        <input name="telephone" defaultValue={this.state.telephone} onChange={this.handleChange} required/>
                    </div>
                    <button className="button bg-graphite" type="submit">Editar</button>
                </form>
            </div>      
        );
    }
}

export default EditUsuarios;