import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify'

class EditSenha extends Component{

    constructor(props) {
        super(props);

        this.state = {
            password: '',
            confirmPassword: ''

        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        let a = this.checkEqual();
        if(a === true){
            this.EditPassword();
        }else{}
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    checkEqual(){
        if(this.state.password === this.state.confirmPassword){
            return true;
        }
        toast.error('As senhas não são iguais');
        return false;
    }

    EditPassword() {

        if(window.confirm('Deseja Editar esta senha?')) {
            Axios({
                method: 'PUT',
                url: `https://ucs.api.life.com.br/user/password/${this.props.user}`,
                data: this.state,
                headers: {'Token':localStorage.getItem('Token')}
            }).then((response) => {
                toast.success(response.data.message)
            }).catch((error) => {
                toast.error(error.message)
            });
        }
    }


    render(){
        return(
            <div>
                <h2>Alterar senha</h2>
                <form id="form" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Nova Senha</label>
                        <input type="password" name="password" defaultValue={this.state.password} onChange={this.handleChange} required />
                        
                    </div>

                    <div className="form-group">
                        <label>Confirmar senha</label>
                        <input type="password" name="confirmPassword" defaultValue={this.state.confirmPassword} onChange={this.handleChange} required/>
                    </div>

                    <button className="button bg-graphite" type="submit">Editar</button>
                </form>
            </div>           
        );
    }
}

export default EditSenha;
