import React, { Component } from 'react';
import './SoftPhone.css';
import SoftPhoneKeyBoard from './SoftPhoneKeyBoard';
import SoftPhoneInCall from './SoftPhoneInCall';

class SoftPhone extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inCall: false,
            numero: ''
        };
        this.callStatus = this.callStatus.bind(this);
        this.getNumero = this.getNumero.bind(this);
    };

    callStatus(status) {
        this.setState({ inCall: status});
    };

    getNumero(numero) {
        this.setState({ numero });
    };
   
    render(){
        if (this.state.inCall === false) {
            return(<SoftPhoneKeyBoard inCall={this.state.inCall} onChange={this.callStatus} numeroProps={this.getNumero}/>);
        } else {
            return(<SoftPhoneInCall inCall={this.state.inCall} onChange={this.callStatus} numero={this.state.numero} />);    
        }
    };      
};

export default SoftPhone;

                            
                            
                            
                            
                            
                            
                           
                            
                            
                            
                            
                            