import React, { Component } from "react";
import Contacts from "../Contacts/Contacts";
import JsSIP from "jssip";
import { LoginContext } from "../../pages/LoginContext";

class SoftPhoneKeyBoard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            numero: "",
            getNumber: this.getNumber
        };

        this.handleChange = this.handleChange.bind(this);
    }

  // funciona teclado virtual
    handleChange(e) {
        if (this.state.numero.length < 11) {
            this.setState({
                numero: this.state.numero + e.target.value
            });
        } else {
            console.log("Limite de caracteres alcançado!");
        }
    }

    handleKeyboard = e => {
        if (e.key === "Enter") {
            // this.makeCall();
        }
        if (e.key === "Backspace") {
            this.setState({ numero: this.state.numero.slice(0, -1) });
        } else {
            if (this.state.numero.length < 11) {
                this.setState({
                    numero: this.state.numero + e.key
                });
            } else {
                console.log("Limite de caracteres alcançado!");
            }
        }
    };

    makeCall() {
        const remoteAudio = new window.Audio();
        remoteAudio.autoplay = true;
        JsSIP.debug.enable("");
        // if (this.state.numero !== '') {
        this.props.numeroProps(this.state.numero);
        this.props.onChange(true);
        const ramal = this.state.numero;

        const socket = new JsSIP.WebSocketInterface();
        const configuration = {};

        const ua = new JsSIP.UA(configuration);

        ua.start();

        const options = {
            mediaConstraints: { audio: true, video: false }
        };

        ua.on("registered", () => {
            ua.call(ramal, options);
        });

        ua.on("newRTCSession", (data) => {
            const session = data.session;
            session.on("confirmed", () => {
                const track = session.connection.getRemoteStreams()[0];
                remoteAudio.srcObject = track;
            });

            session.on("ended", () => {
                console.log("call has ended");
            });
            
            session.on("failed", (e) => {
                console.log("call failed for ", e);
            });
            // session.mute({ audio: true });
            // session.unmute({ audio: true });}
        });
    }

    componentDidMount() {
        document.onkeydown = event => {
            this.handleKeyboard(event);
        };
    }

    render() {
        this.getNumber = (novoNumero) => {
            this.setState({ numero: novoNumero });
        }
        return(
            <div id="phone">
                <Contacts getNumber={this.getNumber} />
                <div className="Ligacao">
                <div className="containerCall">
                    <div className="viewCalling">
                        <h1>{this.state.numero}</h1>
                    <i
                        class="fas fa-backspace"
                        onClick={ () => this.setState({ numero: this.state.numero.slice(0, -1) }) }
                    />
                    </div>
                    <div className="keypad">
                        <table>
                            <tr>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="1"
                                    onClick={this.handleChange}
                                    >
                                    1
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="2"
                                    onClick={this.handleChange}
                                    >
                                    2
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="3"
                                    onClick={this.handleChange}
                                    >
                                    3
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="4"
                                    onClick={this.handleChange}
                                    >
                                    4
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="5"
                                    onClick={this.handleChange}
                                    >
                                    5
                                    </button>
                                </td>
                                <td>
                                    <button
                                        className="buttonP keypadButton"
                                        value="6"
                                        onClick={this.handleChange}
                                    >
                                    6
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="7"
                                    onClick={this.handleChange}
                                    >
                                    7
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="8"
                                    onClick={this.handleChange}
                                    >
                                    8
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="9"
                                    onClick={this.handleChange}
                                    >
                                    9
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button
                                    className="buttonP SpecialButton"
                                    value="*"
                                    onClick={this.handleChange}
                                    >
                                    *
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP keypadButton"
                                    value="0"
                                    onClick={this.handleChange}
                                    >
                                    0
                                    </button>
                                </td>
                                <td>
                                    <button
                                    className="buttonP SpecialButton"
                                    value="#"
                                    onClick={this.handleChange}
                                    >
                                    #
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button
                                    className="buttonP call"
                                    onClick={() => this.makeCall()}
                                    >
                                    <i class="fas fa-phone fa-rotate-90" id="ligar" />
                                    </button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

SoftPhoneKeyBoard.contextType = LoginContext;
export default SoftPhoneKeyBoard;
