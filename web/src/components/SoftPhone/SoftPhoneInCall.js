import React, { Component } from "react";
import Contacts from "../Contacts/Contacts";
import "./SoftPhone.css";

class SoftPhoneInCall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numero: props.numero
        };
    }

    statusCall() {
        this.props.onChange(false);
        this.setState({
            numero: ""
        });
    }

  render() {
        return (
            <div id="phone">
                <Contacts />
                <div className="Ligacao">
                    <div className="containerCall">
                        <h1>In call with...</h1>
                        <div className="numberCall">
                            <h1>{this.state.numero}</h1>
                        </div>
                        <div className="itensCall">
                            <div id="songConf">
                                <i class="fas fa-volume-up"></i>
                                <input type="range" class="slider" />
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <button className="buttonP keypadButton">
                                        <i class="fas fa-microphone-slash" />
                                        </button>
                                    </td>
                                    <td></td>
                                    <td>
                                        <button className="buttonP keypadButton">&#8250;</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <button
                                        className="buttonP turnOff"
                                        onClick={() => this.statusCall()}
                                        >
                                        <i class="fas fa-phone fa-rotate" />
                                        </button>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SoftPhoneInCall;
