import React, { Component } from 'react';
import Contacts from '../Contacts/Contacts';
import './SoftPhone.css';

class SoftPhoneReceivingCall extends Component {
    constructor(props){
        super(props);
        this.state = {
            numero: '(11)977416788'
        };
    };

    statusCall(e) {
        if (e === true){
            this.props.onChange(true);
        } else {
            this.props.onChange(false);
        }
    };

    render() {
        return(
            <div id="phone">
                <Contacts/>
                <div class="Ligacao">
                    <div className="containerReceiving">
                        <h1>Recebendo ligação de:</h1>
                        <h1>{this.state.numero}</h1>
                        <div className="itensCall">
                            <table>
                                <tr>
                                    <td>
                                        <button className="buttonP call" onClick={() => this.statusCall(true)}>
                                            <i class="fas fa-phone fa-rotate-90" id="ligar"/>
                                        </button>
                                    </td>
                                    <td></td>
                                    <td>
                                        <button className="buttonP turnOff receiving" onClick={() => this.statusCall()}>
                                            <i class="fas fa-phone fa-rotate" />
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
};

export default SoftPhoneReceivingCall;