import React, { Component } from 'react';
import './Chat.css';
import Contacts from '../Contacts/Contacts';
import { LoginContext } from '../../pages/LoginContext';
import Axios from 'axios';
import { toast } from 'react-toastify';

class Chat extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            messages: [],
            message: '',
            user: {
                name: undefined,
                telephone: undefined,
                access: undefined,
                id: undefined,
                conversation_id: undefined
            },
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setUser = this.setUser.bind(this);
    }

    getMessages() {
        Axios({
            method: 'POST',
            url: 'https://ucs.api.life.com.br/chat/list',
            headers: {'token': localStorage.getItem('Token') },
            data: {
                conversationId: this.state.user.conversation_id
            }
        }).then((response) => { 
            this.setState({
                messages: response.data
            }, ()=> this.scrollToEnd())
        }).catch((error) => {
            toast.error('Falha ao buscar mensagens!');
        })
    };

    setUser(user) {
        this.setState({
            user: {
                name: user.name,
                telephone: user.telephone,
                access: user.level_id,
                conversation_id: user.conversation_id
            },
            contatoId: user.id
        })
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.addMessageToList();
    }

    addMessageToList() {
        Axios({
            method: 'POST',
            url: 'https://ucs.api.life.com.br/chat/add',
            headers: { 'token': localStorage.getItem('Token') },
            data: {
                text: this.state.message,
                senderId: this.context.id,
                conversationId: this.state.user.conversation_id
            }
        }).then((response) => {
            this.clearInput();
   
        }).catch((error) => {
            toast.error('Erro ao enviar mensagem!');
        });
    }

    clearInput() {
        document.getElementById('message').value = '';
        this.setState({
            message: '',
        });
    }

    isMe(id) {
        if (id === this.context.id) {
            return true;
        } else {
            return false;
        }
    }

    scrollToEnd() {
        const scrollingElement = document.getElementsByClassName("text-box");
        scrollingElement[0].scrollTop = scrollingElement[0].scrollHeight;
        document.getElementById("message").focus();
    }

    componentDidMount(){
        this.interval = setInterval(()=> {
            if (this.state.user.conversation_id !== undefined)
                this.getMessages();
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    
    render() {
        return(
            <div id="chat">
                <Contacts setUser={this.setUser} />
                {this.state.user.conversation_id === undefined ? 
                    <div className="select-conversation">
                        <h1>Selecione uma conversa</h1>
                    </div>
                    : 
                    <div className="conversa">
                    <div className="info-contact">
                        <i class="fas fa-user-circle icon"></i>
                        <h2>{this.state.user.name}</h2>
                    </div>
                        <div className="conversa-box" >
                            <div className="text-box">
                                {this.state.messages.map(msg =>
                                msg.sender_user_id === this.context.id ? 
                                <div className="right">
                                    <label className="white">{msg.text}</label>
                                </div>
                                :
                                <div className="left">
                                    <label className="black">{msg.text}</label>
                                </div>
                            )}
                        </div>
                        <form className="message-group" onSubmit={this.handleSubmit}>
                            <input id="message" name="message" placeholder="Digite sua mensagem" autoComplete="off" onChange={this.handleChange}/>
                            <i type="submit" class="fas fa-paper-plane icon" onClick={()=> this.addMessageToList()}></i>
                        </form>
                    </div>
                </div>}

            </div>
        );
    }
}

Chat.contextType = LoginContext;
export default Chat;