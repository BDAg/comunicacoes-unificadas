import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';
import AddAccessLevel from './AddAccess';
import EditAccessLevel from './EditAccess';
import Modal from '../Modal/Modal';

class ListarNiveisDeAcesso extends Component{

    constructor(props){
        super(props);
        this.state = {
            access: []
        }
    }

    getAccess(){
        Axios({
            method: 'GET',
            url: 'https://ucs.api.life.com.br/access',
            headers: {'Token':localStorage.getItem('Token')}
        }).then((response) => {
            this.setState({
                access: response.data
            })
        }).catch((err) => {
            toast.error(err.message)
        });
    }
    
    deleteAccess(id){
        if(window.confirm('Deseja mesmo remover este nível de acesso?')) {
            Axios({
                method: 'DELETE',
                url: `https://ucs.api.life.com.br/access/${id}`,
                headers: {'Token':localStorage.getItem('Token')}
            }).then((response) => {
                toast.success(response.data.message);
                this.getAccess();
            }).catch((error) => {
                toast.error(error.message)
            });
        }
    }

    componentDidMount() {
        this.getAccess();
    }

    render(){
        return(
            <div class="listContainer">
                <h1>Níveis de Acesso</h1>
                <Modal title={<i className="fas fa-plus-circle addbutton"/>}><AddAccessLevel/></Modal>
                <table>
                    <thead>
                        <th>ID</th>
                        <th>Níveis de Acesso</th>
                    </thead>
                    <tbody>
                        {this.state.access.map(ac =>
                            <tr>
                                <td>{ac.id}</td> 
                                <td>{ac.access}</td>
                                <td ><Modal className="editAccess" title={<i className="far fa-edit tablebutton"/>}><EditAccessLevel ac={ac.id} /></Modal></td>
                                <td><a type="submit" onClick={() => this.deleteAccess(ac.id)}><i className="fas fa-trash-alt tablebutton"/></a></td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ListarNiveisDeAcesso;