import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';

class AddAccessLevel extends Component { 
    constructor(props) {
        super(props);

        this.state = {
            access: '',
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.addAccess();
    }

    addAccess() {
        Axios({
            method: 'POST',
            url: 'https://ucs.api.life.com.br/access',
            headers: { 'Token': localStorage.getItem('Token') },
            data: this.state
        }).then((response) => {
            toast.success(response.data.message)
        }).catch((error) => {
            toast.error(error.message)
        })
    }

    render() {
        return(
            <div>
                <h1>Adicionar Nível de Acesso</h1>
                <form id="form" onSubmit={this.handleSubmit}>
                    <label>Acesso</label>
                    <input name="access" id="access" onChange={this.handleChange}/>
                    <button className="button bg-graphite" type="submit">Enviar</button>
                </form>
            </div>
        );
    }
}

export default AddAccessLevel;