import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';

class EditAccessLevel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            access: '',
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }
    componentDidMount() {
        this.getAccess();
    }
    handleSubmit(event) {
        event.preventDefault();
        this.editAccess();
    }
    getAccess() {
        Axios({
            method: 'GET',
            url: `https://ucs.api.life.com.br/access/${this.props.ac}`,
            headers: { 'Token': localStorage.getItem('Token') }
        }).then((response) => {
            this.setState({
                access: response.data
            })
            console.log(this.state.access)
        }).catch((err) => {
            toast.error(err.message)
        });
    }

    editAccess() {

        if (window.confirm('Deseja alterar este nível ?')) {
            Axios({
                method: 'PUT',
                url: `https://ucs.api.life.com.br/access/${this.props.ac}`,
                headers: { 'Token': localStorage.getItem('Token') },
                data: this.state
            }).then((response) => {
                toast.success(response.data.message)
            }).catch((error) => {
                toast.error(error.message)
            })
        }
    }
    render() {
        return (
            <div>
                <h1>Editar Nível de Acesso</h1>
                <form id="form" onSubmit={this.handleSubmit}>
                    <label>Acesso</label>
                    <input name="access"  id="access" onChange={this.handleChange} />
                    <button className="button bg-graphite" type="submit">Alterar</button>
                </form>
            </div>
        );
    }
}

export default EditAccessLevel;