import React, { Component } from 'react';   
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Alert extends Component{

    render(){
        return(
            <div>
                <ToastContainer 
                position= "bottom-center"
                autoClose= {5000}
                showProgressBar= {true}
                closeOnClick= {true}
                pauseOnHover= {false}
                draggable= {false}
                closeButton= {false}/>
            </div>
        );
    }

} 

export default Alert;
