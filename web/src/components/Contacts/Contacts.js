import React, { Component } from 'react';
import './Contacts.css';
import Axios from 'axios';
import { toast } from 'react-toastify';
import { LoginContext } from '../../pages/LoginContext';

class Contacts extends Component {
    constructor(props) {
        super(props);
        this.state= {
            contatos: []
        };
    };

    handleService(user) {
        if (document.getElementById('phone') == null) {
            console.log('chat');
            this.props.setUser(user);
            if (user.visualized === 0) {
                this.setMessagesVisualized(user);
            }
            
        } else {
            if (document.getElementById('songConf') == null) {
                this.props.getNumber(user.telephone);
            } else {
                console.log('Em Ligação');
            };
        }
    }

    getContacts() {
        Axios({ 
            method: 'GET',
            url: `https://ucs.api.life.com.br/user/conversation/${this.context.id}`,
            headers: { 'Token': localStorage.getItem('Token')}
        }).then((response) => {
            this.setState({
                contatos: response.data
            })
        }).catch((error) => {
            toast.error(error.response.data.message);
        })
    };

    setMessagesVisualized(user) {
        Axios({
            method: 'POST',
            url: `https://ucs.api.life.com.br/chat/visualized`,
            headers: { 'Token': localStorage.getItem('Token') },
            data: {
                conversation_id: user.conversation_id
            }
        }).then((response) => {
            console.log('todas msg foram lidas')
            document.getElementById(user.user_id).style.display = "none";
        }).catch((error) => {
            toast.error(error.response.data.message);
        })
    }

    componentDidMount() {
        this.getContacts();
    };

    render() {
        return(
            <div id="contacts">
                <div className="header-contacts">
                    <i class="fas fa-bars icon"/>
                    <h1>Contatos</h1>
                </div>
                <div className="contact-box">
                    {this.state.contatos.map(user =>
                        user.user_id !== this.context.id ?
                        <div key={user.id} className="contact-group" onClick={() => this.handleService(user)}>
                            <i class="fas fa-user-circle icon" />
                            <div class="contact-name">
                                {user.name}
                                {!user.visualized && !document.getElementById('phone') ? <div id={user.user_id} className="new-message" /> : ''}
                            </div>
                        </div> : ''
                    )}
                </div>
            </div>
        )
    }
};

Contacts.contextType = LoginContext;
export default Contacts;