import React, { Component } from "react";
import { Switch, Route, HashRouter } from "react-router-dom";
import ControlRouter from "./ControlRouter";
import Layout from "./layouts/Layout";
import User from "./pages/User";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Access from "./pages/Access";
import Softphone from "./pages/Phone";
import Chat from "./pages/Chat";
import { LoginContext } from "./pages/LoginContext";
import Axios from "axios";

class App extends Component {
    constructor(props) {
        super(props);

        this.getUser = token => {
            Axios({
                method: "GET",
                url: "https://ucs.api.life.com.br/user/token/find",
                headers: { token: token }
            }).then(response => {
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    access: response.data.access,
                    telephone: response.data.telephone
                });
            }).catch(error => {
                console.log("Erro ao setar info de usuario!");
            });
        };

        this.state = {
            id: undefined,
            name: undefined,
            access: undefined,
            telephone: undefined,
            getUser: this.getUser
        };
    }

    getToken() {
        const token = localStorage.getItem("Token");

        if (token) {
            this.getUser(token);
        }
    }

    componentDidMount() {
        this.getToken();
    }

    render() {
        return (
            <LoginContext.Provider value={this.state}>
                <HashRouter>
                    <Switch>
                        <ControlRouter
                        exact
                        path="/user"
                        layout={Layout}
                        component={User}
                        accessLevel="3"
                        />
                        <ControlRouter
                        exact
                        path="/home"
                        layout={Layout}
                        component={Home}
                        accessLevel="1"
                        />
                        <ControlRouter
                        exact
                        path="/access"
                        layout={Layout}
                        component={Access}
                        accessLevel="3"
                        />
                        <ControlRouter
                        exact
                        path="/phone"
                        layout={Layout}
                        component={Softphone}
                        accessLevel="1"
                        />
                        <ControlRouter
                        exact
                        path="/chat"
                        layout={Layout}
                        component={Chat}
                        accessLevel="1"
                        />
                        <Route exact path="/" component={Login} />
                    </Switch>
                </HashRouter>
            </LoginContext.Provider>
        );
    }
}

export default App;
