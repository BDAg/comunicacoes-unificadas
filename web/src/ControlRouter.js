import React, { Component } from 'react';
import Loading from './components/Loading'
import { LoginContext } from './pages/LoginContext'
import { Redirect } from 'react-router-dom';

class ControlRouter extends Component {
    
    constructor(props){
        super(props);
            this.state = {
                loaded: false,
        };
    }

    componentWillMount(){
        setTimeout(() => {
            this.setState({
              loaded: true,
            });
          }, 1500)
    }

    render() {
        if (this.state.loaded === true){
            const {
                component: Component,
                layout: Layout,
                accessLevel: access,
                ...rest
            } = this.props;

            let renderComponent = undefined;
            let token = localStorage.getItem('ValueToken');
            
            if (token !== undefined) {
                if (this.context.access !== undefined) {
                    if (this.context.access < this.props.accessLevel){
                        renderComponent = <Redirect to='/home'/>;
                        console.log('level de acesso muito baixo')
                    }else{
                        renderComponent = <Layout><Component {...rest} /></Layout>;
                    }
                } else {
                    renderComponent = <Redirect to='/'/>;
                    console.log('level de acesso indefinido')
                }
            } else {
                renderComponent = <Redirect to='/'/>;
                console.log('token não gerado/expirado')
            }


            return (
            <div id="app">{ renderComponent }</div>
            );
        } else {
            return <Loading loaded='false'/>
        }
    }
}

ControlRouter.contextType = LoginContext;
export default ControlRouter;

