import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';
import { LoginContext } from '../pages/LoginContext';

class LoginScreen extends Component{

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    componentDidMount() {
        document.getElementById("email").focus();
    }

    authControll(){
        Axios({
            method: 'POST',
            url: 'https://ucs.api.life.com.br/auth',
            data: this.state
        }).then((response) => {
            localStorage.setItem('Token', response.data.token);
            this.context.getUser(response.data.token);
            this.props.history.push('/home');
        }).catch((err) => {
            console.log(err);
            toast.error('Preencha todos os campos!');
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        this.authControll();
    }

    render(){
        
        return(
            <div class='loginPage'>
                <div class='logContainer'>
                    <form onSubmit={this.handleSubmit}>
                        <img src='/img/comunica2.png' alt="Logo life Login" width="300"/>
                        <input placeholder='Username' type="email" id="email" name="email" onChange={this.handleChange}required></input>
                        <input placeholder='Password' type="Password" id="password" name="password" onChange={this.handleChange}></input>
                        <button className="button bg-graphite" type='submit'>Login</button>
                    </form>
                </div>
            </div>
        );
    }
}

LoginScreen.contextType = LoginContext;
export default LoginScreen;