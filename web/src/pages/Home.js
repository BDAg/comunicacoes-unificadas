import React, { Component } from 'react';

class Home extends Component {
    render() {
        return(
            <div className="title-home">
                <h1>SEJA BEM VINDO À PLATAFORMA DE COMUNICAÇÕES UNIFICADAS!</h1>
            </div>
        );
    }
}

export default Home;