import db from '../config/database';
import moment from 'moment-timezone';

class ChatModel {
    static insertConversation() {
        const dateStart = moment.tz('America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');
        const query = `INSERT INTO conversation (dateStart) VALUES ('${dateStart}')`;

        return new Promise ((resolve, reject) => {
            db.query(query, (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static insertUserConversation(userId, conversationId) {
        const query = `INSERT INTO user_conversation (user_id, conversation_id) VALUES (?, ?)`;
        const data = [userId, conversationId];

        return new Promise((resolve, reject) => {
            db.query(query, data, (err, result) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(result);
                }
            });
        });
    }

    static insertMessage(text, senderUserId, conversationId) {
        const dateSend = moment.tz('America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');
        const query = `INSERT INTO message (text, dateSend, sender_user_id, conversation_id, visualized) VALUES (?, ?, ?, ?, 0)`;
        const data = [text, dateSend, senderUserId, conversationId];

        return new Promise((resolve, reject) => {
            db.query(query, data, (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static getMessages(conversationId) {
        const query = `SELECT M.id, M.text, M.dateSend, M.sender_user_id, M.conversation_id
        FROM message M
        WHERE conversation_id = ${conversationId}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static updateMessageSetVisualized(conversationId) {
        const query = `UPDATE message SET visualized = 1 WHERE conversation_id = ${conversationId}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }
}

export default ChatModel;