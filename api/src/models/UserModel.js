import db from '../config/database';
import moment from 'moment-timezone';

class UserModel {
    static add(name, telephone, email, level_id, password, active) {
        const query = `INSERT INTO user (name, telephone, email, level_id, password, active) VALUES ('${name}', '${telephone}', '${email}', '${level_id}', '${password}', '${active}')`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static get(idUser) {
        const query = `SELECT * FROM user WHERE id = ${idUser}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result[0]);
                }
            });
        });
    }

    static list() {
        const query = `SELECT * FROM user`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static edit(idUser, name, telephone, email, level_id) {
        const query = `UPDATE user SET name = '${name}', telephone = '${telephone}', email = '${email}', level_id = '${level_id}' WHERE id = ${idUser}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static delete(idUser) {
        const query = `DELETE FROM user WHERE id = ${idUser}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static editPassword(idUser, password) {
        const query = `UPDATE user SET password='${password}' WHERE id = ${idUser}`;
        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static updateActive(idUser, active) {
        const query = `UPDATE user SET active='${active}' WHERE id = ${idUser}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static getUsersByUserId(userId) {
        const query = `SELECT  U.id as user_id, U.name, U.telephone, U.level_id, USC.conversation_id, CASE WHEN 0 IN (
                SELECT M.visualized 
                FROM message M 
                WHERE M.conversation_id = USC.conversation_id
            )
            THEN false
            ELSE true
            END as 'visualized'
        FROM user_has_conversation USC 
        JOIN user U on U.id = USC.user_id 
        WHERE conversation_id IN (SELECT conversation_id FROM user_has_conversation WHERE user_id = ${userId}) AND user_id != ${userId} ORDER BY visualized`;

        return new Promise ((resolve, reject) => {
            db.query(query, (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static getUserByNotUser(userId) {
        const query  = `SELECT * FROM user WHERE user.id != ${userId}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    }

    static insertConversation() {
        const date = moment.tz('America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');
        const query = `INSERT INTO conversation (dateStart) VALUES ('${date}')`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    static insertUserConversation(userId, newUser, conversationId) {
        const query = `INSERT INTO user_has_conversation (user_id, conversation_id) VALUES (${userId}, ${conversationId}), (${newUser}, ${conversationId})`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    }
};

export default UserModel;