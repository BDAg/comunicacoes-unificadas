import db from '../config/database';

class AccessModel {
    static add(access){
        const query = `INSERT INTO level (access) VALUES ('${access}')`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    };

    static get(idLevel){
        const query = `SELECT * FROM level WHERE id = ${idLevel}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    };
    
    static list(){
        const query = `SELECT * FROM level`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    };
    
    static edit(idLevel, access){
        const query = `UPDATE level SET access = '${access}' WHERE id = ${idLevel}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    };

    static delete(idLevel){
        const query = `DELETE FROM level WHERE id = ${idLevel}`;

        return new Promise((resolve, reject) => {
            db.query(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        })
    };
    
}

export default AccessModel;