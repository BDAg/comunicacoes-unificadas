import UserModel from '../models/UserModel';
import bcrypt from 'bcryptjs';
import JWT from 'jsonwebtoken';
import config from '../config/config';

class UserController {
    
    static add(req, res) {
        UserModel.add(req.body.name, req.body.telephone, req.body.email, req.body.level_id, bcrypt.hashSync(req.body.password), 1).then((response) => {   
            UserModel.getUserByNotUser(response.insertId).then((responseGet) => {
                for (let user of responseGet) {
                    UserModel.insertConversation().then((responseConversation) => {
                        UserModel.insertUserConversation(user.id, response.insertId, responseConversation.insertId)
                        .catch((errorInsert1) => {
                            res.status(500).send(errorInsert1);
                            return;
                        })
                    }).catch((errorC) => {
                        res.status(500).send(errorC);
                        return;
                    });
                }
                res.status(200).send({ message: 'Usuario inserido com sucesso!' })
            }).catch((error) => {
                res.status(500).send(error)
            })
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao cadastrar usuário!', error });
        });
    }

    static get(req, res) {
        UserModel.get(req.params.id).then((response) => {
            res.status(200).send(response);
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao encontrar usuário!', error });
        });
    }

    static list(req, res) {
        UserModel.list().then((response) => {
            if (response.length > 0) {
                res.status(200).send(response);
            } else {
                res.status(404).send({ message: 'Nehum usuário cadastrado!' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao encontrar usuários!', error });
        });
    }    

    static edit(req, res) {
        UserModel.edit(req.params.id, req.body.name, req.body.telephone, req.body.email, req.body.level_id).then((response) => {
            if (response.affectedRows > 0) {
                res.status(200).send({ message: 'Usuário editado com sucesso!', response });
            } else {
                res.status(404).send({ message: 'Usuário não encontrado!' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao editar usuário!', error });
        });
    }

    static delete(req, res) {
        UserModel.delete(req.params.id).then((response) => {
            if (response.affectedRows > 0) {
                res.status(200).send({ message: 'Usuário deletado com sucesso!', response });
            } else {
                res.status(404).send({ message: 'Falha ao encontrar usuário' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao deletar usuario!', error });
        });
    }

    static editPassword(req, res) {
        UserModel.editPassword(req.params.id, bcrypt.hashSync(req.body.password)).then((response) => { 
            res.status(200).send({ message: 'Senha alterada com sucesso!', response});
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao alterar senha!', error});
        });
    }

    static updateActive(req, res) {
        UserModel.updateActive(req.params.id, req.body.active).then((response) => {
            res.status(200).send({ message: 'Active alterado com sucesso!', response });
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao alterar active!', error})
        })
    }

    static getUserByToken(req, res) {
        const token = req.headers['token'];

        JWT.verify(token, config.secret, (err, decoded) => {
            if (err) {
                res.status(500).send(err);
            } else {
                UserModel.get(decoded.id).then((response) => {
                    res.status(200).send({ access: response.level_id, name: response.name, id: response.id, telephone: response.telephone });
                }).catch((error) => {
                    res.status(500).send(error);
                })
            }
        })
    }

    /**Busca usuários que estão em uma conversa com esse usuario (logado) */
    static listUsersByUserId(req, res) {
        UserModel.getUsersByUserId(req.params.id).then((response) => {
            res.status(200).send(response);
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao listar usuários', error });
        });
    }
}

export default UserController;