import config from '../config/config';
import JWT from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import AuthModel from '../models/AuthModel';

class AuthController {

    static login(req, res) {
        AuthModel.getUser(req.body.email).then((response) => {
            if(bcrypt.compareSync(req.body.password, response.password)) {
                const token = JWT.sign({ 
                    id: response.id,
                    name: response.name,
                    email: response.email,
                    telephone: response.telephone
                }, config.secret, { expiresIn: 60 * 60 });

                res.status(200).send({ auth: true, token });
            } else {
                res.status(401).send({ auth: false, token: null });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Erro ao realizar Login!', error });
        });
    }
}

export default AuthController;