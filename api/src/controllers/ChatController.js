import ChatModel from '../models/ChatModel';

/**
 * @param req.body.text
 * @param req.body.user_id
 */
class ChatController {
    static addMessage(req, res) {
        ChatModel.insertMessage(req.body.text, req.body.senderId, req.body.conversationId).then((response) => {
            res.status(200).send({ message: 'ok' });
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao adicionar mensagem!', error });
        });
    }

    static listMessages(req, res) {
        ChatModel.getMessages(req.body.conversationId).then((response) => {
            res.status(200).send(response);
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao listar mensagens!' });
        });
    }

    static setMessagesVisualized(req, res) {
        ChatModel.updateMessageSetVisualized(req.body.conversation_id).then((response) => {
            res.status(200).send({ message: `Mensagens da conversa ${req.body.conversation_id} marcadas como visualizadas!` });
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao marcar mensagens como visualizadas!', error });
        });
    }
}

export default ChatController;