import AccessModel from '../models/AccessModel';

class AccessController {

    static add(req, res){
        if (req.body.access == undefined || req.body.access == null) {
            res.status(404).send({ message: 'Nível de acesso não informado!' })            
        } else {
            AccessModel.add(req.body.access).then((response) => {
                res.status(200).send({ message: 'Nível de acesso adicionado com sucesso!', response})
            }).catch((error) => {
                res.status(500).send({ message: 'Falha ao adicionar nível de acesso!', error})
            });
        }
    };

    static get(req, res){
        AccessModel.get(req.params.id).then((response) => {
            if (response.length > 0) {
                res.status(200).send(response);
            } else {
                res.status(404).send({ message: 'Id de nível de acesso não encontrado!' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao buscar nível de acesso!', error })
        });
    };

    static list(req, res){
        AccessModel.list().then((response) => {
            if (response.length > 0) {
                res.status(200).send(response);
            } else {
                res.status(404).send({ message: 'Nehum nível de acesso encontrado!' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao buscar níveis de acesso!', error });
        });
    };

    static edit(req, res){
        AccessModel.edit(req.params.id, req.body.access).then((response) => {
            if (response.affectedRows > 0) {
                res.status(200).send({ message: 'Nível de acesso alterado com sucesso!' });
            } else {
                res.status(404).send({ message: 'Nível de acesso não encontrado!' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao editar nível de acesso!' });
        })
    };

    static delete(req, res){
        AccessModel.delete(req.params.id).then((response) => {
            if (response.affectedRows > 0) {
                res.status(200).send({ message: 'Nível de acesso deletado com sucesso!', response });
            } else {
                res.status(404).send({ message: 'Falha ao encontrar nível de acesso!' });
            }
        }).catch((error) => {
            res.status(500).send({ message: 'Falha ao deletar nível de acesso!', error });
        });
    };

}

export default AccessController;