import express from 'express';
import bodyParser from 'body-parser';
import CORS from 'cors'; // evitar bug quando ha mais de um porta executando
import helmet from 'helmet'; // garantir segurança no geral (sql inject)
import compression from 'compression';
import User from './routes/User';
import Access from './routes/Access';
import Auth from './routes/Auth';
import Chat from './routes/Chat';

const app = express(); 

app.use(CORS());
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false }));

app.use('/user', User);
app.use('/access', Access);
app.use('/auth', Auth);
app.use('/chat', Chat);

app.listen(8080);