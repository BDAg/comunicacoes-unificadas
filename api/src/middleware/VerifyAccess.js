import JWT from 'jsonwebtoken';
import config from '../config/config';
import UserModel from '../models/UserModel';

export default function Access(level) {
    return (req, res, next) => {
        const token = req.headers['token'];

        if(!token) {
            return res.status(403).send({ auth: false, message: 'Token não informado!' })
        } else {
       
            JWT.verify(token, config.secret, (err, decoded) => {
                if (err) { 
                    return res.status(500).send({ auth: false, message: 'Falha ao autenticar' });
                } else {
                    req.user = decoded.id;

                    UserModel.get(decoded.id).then((response) => {
                        if(response.level_id >= level) {
                            next();
                        } else {
                            return res.status(401).send({ auth: false, message: 'Não autorizado!' })
                        }
                    }).catch((error) => {
                        return res.status(500).send({ error });
                    });
                }
            });
        }
    }
}