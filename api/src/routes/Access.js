import AccessController from '../controllers/AccessController';
import { Router } from 'express';
import Access from '../middleware/VerifyAccess';

const Route = Router();

Route.post('/', Access(1), AccessController.add);
Route.get('/:id', Access(1), AccessController.get);
Route.get('/', Access(2), AccessController.list);
Route.put('/:id', Access(3), AccessController.edit);
Route.delete('/:id', Access(3), AccessController.delete);

export default Route;