import { Router } from 'express';
import Access from '../middleware/VerifyAccess';
import ChatController from '../controllers/ChatController';


const Route = Router();

Route.post('/add', Access(1), ChatController.addMessage);
Route.post('/list', Access(1), ChatController.listMessages);
Route.post('/visualized', Access(1), ChatController.setMessagesVisualized);

export default Route;