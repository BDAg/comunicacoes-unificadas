import UserController from '../controllers/UserController';
import { Router } from 'express';
import Access from '../middleware/VerifyAccess';

const Route = Router();

Route.post('/', Access(1), UserController.add);
Route.get('/:id', Access(1), UserController.get);
Route.get('/', Access(1), UserController.list);
Route.put('/:id', Access(1), UserController.edit);
Route.delete('/:id', Access(1), UserController.delete);
Route.put('/password/:id',Access(1), UserController.editPassword);
Route.put('/active/:id', Access(1), UserController.updateActive);
Route.get('/token/find', Access(1), UserController.getUserByToken);
Route.get('/conversation/:id', Access(1), UserController.listUsersByUserId);

export default Route;